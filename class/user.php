<?
class wp_login__class__user extends wp_login__class__user__parent
{
	function login($d=null)
	{
		parent::{__function__}($d);
		$this->D['USER']['W']['EMAIL|NICK'] = $this->D['LOGIN']['NICK'];
		$this->get_user();
		$kU = array_keys((array)$this->D['USER']['D']);
		if(isset($kU[0]) && password_verify($this->D['LOGIN']['PASSWORD'],$this->D['USER']['D'][ $kU[0] ]['PASSWORD']))
		{
			$this->D['SESSION']['USER'] = $this->D['USER']['D'][ $kU[0] ];
			$this->D['SESSION']['USER']['ID'] = $kU[0];
			header("Location: {$this->D['SESSION']['LOGIN']['BACK_REDIRECT']}");
			exit;
		}
		else
			$this->D['ERROR'] = '0001';
	}
	
	function logout($d=null)
	{
		parent::{__function__}($d);
		unset($_SESSION['D']['USER']);
	}
} 