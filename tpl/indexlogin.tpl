{block name="<body>"}
{if !$D.SESSION.USER.ID}
<form action="?D[PAGE]=indexlogin&D[ACTION]=login&D[LASTPAGE]={$D.LASTPAGE}" method="post">
	<div class="panel panel-default" style="z-index:999;position:relative;width:400px;top:100px;margin:auto;">
		<div class="panel-body">
			{if $D.ERROR}
				<div class="alert alert-warning" role="alert">##{$D.ERROR}##</div>
			{/if}
			<div class="input-group">
				<span class="input-group-addon"><div style="width:50px;"><i class="fa fa-user" aria-hidden="true"></i> / <i class="fa fa-envelope" aria-hidden="true"></i></div></span>
				<input type="text" name="D[LOGIN][NICK]" value="{$D.LOGIN.NICK}" class="form-control" placeholder="Username / E-Mail">
			</div>
			<div class="input-group">
				<span class="input-group-addon"><div style="width:50px;"><i class="fa fa-key" aria-hidden="true"></i></div></span>
				<input type="password" name="D[LOGIN][PASSWORD]" value="{$D.LOGIN.PASSWORD}" class="form-control" placeholder="Passwort">
				<span class="input-group-btn">
					<button class="btn btn-default" type="button"><i class="fa fa-question" aria-hidden="true"></i>&nbsp;</button>
				</span>
			</div>
			<div class="input-group" style="width:100%;">
				<button type="submit" class="btn btn-default" style="width:100%;"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</button>
			</div>
		</div>
	</div>
</form>
{else}
	<button type="button" onclick="window.open('?D[PAGE]=indexlogin&D[ACTION]=logout','_self');" class="btn btn-default">Logout</button>
{/if}
{/block}