<?
class wp_login__indexlogin extends wp_login__indexlogin__parent
{
	function load($d = null)
	{
		switch($this->D['ACTION'])
		{
			case 'login':
				$this->C->user()->login();
				break;
			case 'logout':
				$this->C->user()->logout();
				break;
		}
		
		$this->D['TEMPLATE']['FILE'][] = basename(__file__,'.php').".tpl";
		parent::{__function__}();
		$this->C->template()->display();
		
		#$this->C->library()->smarty()->assign('D', $this->D);
		#$this->C->library()->smarty()->display(__dir__.'/tpl/indexlogin.tpl');
	}
}